const http = require('./config')

module.exports = {
    listar: nome => nome ? http.get('avenidas?nome=' + nome) : http.get('avenidas'),
    registrar: avenida => http.post('avenidas', avenida),
    excluir: id => http.delete('avenidas', {data: {id: id}})
}